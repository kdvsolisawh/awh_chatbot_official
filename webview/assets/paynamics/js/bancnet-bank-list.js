﻿function GetBankValue(val) {

    switch (val) {
        case "Allied Banking Corporation":
            document.getElementById("atmDigits").value = "504904, 329";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCardName").innerHTML = "Allied Bank";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_allied.png";
            break;

        case "Allied Savings Bank":
            document.getElementById("atmDigits").value = "606293";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "Allied Bank";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_allied.png";
            break;

        case "Asia United Bank":
            document.getElementById("atmDigits").value = "602914";
            document.getElementById("maxLimit").value = "100,000.00 (combined w/ Bill payments)";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Banco Dipolog":
            document.getElementById("atmDigits").value = "581674";
            document.getElementById("maxLimit").value = "40,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Bank of Commerce":
            document.getElementById("atmDigits").value = "590144";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "BPI Direct BanKo":
            document.getElementById("atmDigits").value = "590118";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "CTBC Bank":
            document.getElementById("atmDigits").value = "0069, 603224, 421433, 421434, 421562, 421563, 421564, 421565, 421665, 428339, 428307, 248303, 449174, 421679, 428346, 428347, 428353, 428354, 428355, 428356";
            document.getElementById("maxLimit").value = "100,000.00 (combined w/ ATM withdrawal and IBFT)";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "CTBC Bank";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_ctcb.png";
            break;

        case "Citystate Savings Bank":
            document.getElementById("atmDigits").value = "601959";
            document.getElementById("maxLimit").value = "40,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "DBP":
            document.getElementById("atmDigits").value = "200059, 470676, 466536, 502277, 455366";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Dumaguete City Dev. Bank":
            document.getElementById("atmDigits").value = "581837";
            document.getElementById("maxLimit").value = "50,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "East West Bank":
            document.getElementById("atmDigits").value = "437507";
            document.getElementById("maxLimit").value = "50,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Enterprise Bank":
            document.getElementById("atmDigits").value = "505813";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Entrepreneur Bank":
            document.getElementById("atmDigits").value = "581870";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Equicom Savings Bank":
            document.getElementById("atmDigits").value = "428428, 428410, 464966, 461766, 461764, 461762, 461761";
            document.getElementById("maxLimit").value = "-";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Greenbank Inc.":
            document.getElementById("atmDigits").value = "639179";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "ISLA Bank":
            document.getElementById("atmDigits").value = "581678";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Korea Exchange Bank":
            document.getElementById("atmDigits").value = "581869";
            document.getElementById("maxLimit").value = "50,000.00 combined w/ Withdrawal/IBFT & Bills Payment";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Landbank":
            document.getElementById("atmDigits").value = "474844, 603131, 2038";
            document.getElementById("maxLimit").value = "100,000";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "Landbank";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_landbank.png";
            break;

        case "Luzon Development Bank":
            document.getElementById("atmDigits").value = "601652";
            document.getElementById("maxLimit").value = "40,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Malayan Savings Bank":
            document.getElementById("atmDigits").value = "603187";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "MASS SPECC":
            document.getElementById("atmDigits").value = "639403";
            document.getElementById("maxLimit").value = "-";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Maybank":
            document.getElementById("atmDigits").value = "603324";
            document.getElementById("maxLimit").value = "30,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Metrobank":
            document.getElementById("atmDigits").value = "504946, 589282, 263";
            document.getElementById("maxLimit").value = "Bank to provide together with their confirmation on the BINs";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "Metrobank";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_metrobank.png";
            break;

        case "PBCom":
            document.getElementById("atmDigits").value = "589886";
            document.getElementById("maxLimit").value = "50,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "PSBank":
            document.getElementById("atmDigits").value = "627592";
            document.getElementById("maxLimit").value = "50,000.00 (cardholders can request for higher limit)";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "PBB":
            document.getElementById("atmDigits").value = "605053";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "PNB":
            document.getElementById("atmDigits").value = "510266, 528204, 592108";
            document.getElementById("maxLimit").value = "20,000 – 100,000 / Shared with ATM Limits";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "PVB":
            document.getElementById("atmDigits").value = "590133";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Pacific Ace Savings Bank":
            document.getElementById("atmDigits").value = "627227";
            document.getElementById("maxLimit").value = "-";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Philtrust Bank":
            document.getElementById("atmDigits").value = "200000, 627507";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "Philtrust Bank";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_philtrust.png";
            break;

        case "Planbank":
            document.getElementById("atmDigits").value = "581827";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Phil. Postal Savings Bank":
            document.getElementById("atmDigits").value = "627818";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Philippine Veterans Bank":
            document.getElementById("atmDigits").value = "5901334, 5901339, 5901337, 5901336, 5901338, 5901335, 5901333, 5901332, 5901331, 5901330";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "RCBC":
            document.getElementById("atmDigits").value = "421585, 589270, 601971, 8888";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "block";
            document.getElementById("bankCardName").innerHTML = "RCBC";
            document.getElementById("bankCardImg").src = "images/bancnet-banks/bn_rcbc.png";
            break;

        case "RSB":
            document.getElementById("atmDigits").value = "581816, 590160";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Robinsons Savings Bank":
            document.getElementById("atmDigits").value = "603080";
            document.getElementById("maxLimit").value = "40,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Security Bank":
            document.getElementById("atmDigits").value = "140651";
            document.getElementById("maxLimit").value = "40,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Standard Chartered Bank":
            document.getElementById("atmDigits").value = "601730, 407380, 460186";
            document.getElementById("maxLimit").value = "100,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Sterling Bank of Asia":
            document.getElementById("atmDigits").value = "604885, 461781, 461780, 461769, 461782, 418903, 461753, 483692";
            document.getElementById("maxLimit").value = "No Limit";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Sun Savings Bank":
            document.getElementById("atmDigits").value = "581880";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "Tiaong Rural Bank":
            document.getElementById("atmDigits").value = "588858";
            document.getElementById("maxLimit").value = "20,000";
            document.getElementById("bankCard").style.display = "none";
            break;

        case "":
            document.getElementById("atmDigits").value = "-";
            document.getElementById("maxLimit").value = "-";
            document.getElementById("bankCard").style.display = "none";
            break;
    }
}