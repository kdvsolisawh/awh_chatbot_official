//http://www.prepbootstrap.com/bootstrap-template/credit-card-payment


$('#myModal').on('show.bs.modal', function (event) {
    console.log("OK");
    let data_id = $(event.relatedTarget).data('id') 
    console.log(medData[data_id]);
    document.getElementsByClassName("modal-body")[0].innerHTML = "Name: " + medData[data_id].name + "<br/>" + 
                                                                 "Dosage: " + medData[data_id].dosage + "<br/>" +
                                                                 "Form: " + medData[data_id].form + "<br/>" +
                                                                 "Package Size: " + medData[data_id].packageSize + "<br/>" +
                                                                 "Ingredients: " + medData[data_id].ingredients.join("<br/>") + "<br/>" +
                                                                 "Tax: " + medData[data_id].tax + "<br/>" +
                                                                 "Shipping Cost: " + medData[data_id].shipping_cost + "<br/>" +
                                                                 "Unit Price: " + medData[data_id].price + "<br/>";
 });

$( "button.btn-process" ).click(function() {
    console.log("click")
    var el = document.querySelector(".success-payment-message");
    el.innerHTML = "<img src='images/loading.gif'> Verifying payment...";

    var tokenGetter = new XMLHttpRequest();
    tokenGetter.open("POST", "/ccard-token", true);
    tokenGetter.setRequestHeader('Content-Type', 'application/json');
    tokenGetter.send(JSON.stringify({
        "type": "card",
        "number": document.getElementsByClassName("form-control")[0].value,
        "expiry_month" : document.getElementsByClassName("form-control")[1].value.substring(0,2),
        "expiry_year" : document.getElementsByClassName("form-control")[1].value.substring(5),
        "cvv" : document.getElementsByClassName("form-control")[2].value,
        "name" : document.getElementsByClassName("form-control")[3].value,
      }
    ));
    tokenGetter.onload = function() {
        var data = JSON.parse(this.responseText);
        if(typeof data.token !== 'undefined' && data.token !== 'error') {
            var paymentProcessor = new XMLHttpRequest();
            paymentProcessor.open("POST", "/ccard-payment", true);
            paymentProcessor.setRequestHeader('Content-Type', 'application/json');
            paymentProcessor.send(JSON.stringify({ 
                    "source": { 
                        "type": "token", 
                        "token": data.token
                    },
                    "amount": total_price.replace(".", ""),
                    "currency": currency,
                    "reference": "12345678"//payment_id
                }
            ));
            paymentProcessor.onload = function() {
                console.log(this.responseText)
                var data = JSON.parse(this.responseText);
                if(data.status == "Authorized") {
                    el.innerHTML = "Payment Complete!";
                    var notify = new XMLHttpRequest();
                    notify.open("POST", restUrl + "/notify_order", true);
                    notify.setRequestHeader('Content-Type', 'application/json');
                    notify.send(JSON.stringify({
                        "messenger_id" : messenger_id,
                        "blockName" : "Receipt",
                        "attributes" : {
                            "payment_id":"12345",//payment_id,
                            "payment_method":"ccard",
                            "patientName":patientName,
                            "address":address,
                            "elements":elements,
                            "total_tax":total_tax,
                            "total_shipping_cost":total_shipping_cost,
                            "discount":adjustments,
                            "total_cost": total_price
                        }
                    }));
                    var confirm = new XMLHttpRequest();
                    confirm.open("POST", restUrl + "/notify_order", true);
                    confirm.setRequestHeader('Content-Type', 'application/json');
                    confirm.send(JSON.stringify({
                        "messenger_id" : messenger_id,
                        "blockName" : "Confirmation",
                        "attributes" : { }
                    }));
                    ME.requestCloseBrowser(function() {
                        console.log('Window will be closed');
                    }, function(error) {
                        console.log('There is an error');
                        console.log(error);
                    });
                } else {
                    el.innerHTML = "Payment Error! Please try again";
                    location.reload(); 
                }
            }
        } else {
            el.innerHTML = "Payment Error! Please try again";
            location.reload(); 
        }
    }
/*
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/ccard-payment", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    //xhr.setRequestHeader('Authorization', checkout_secret_key);
    console.log(document.getElementsByClassName("form-control"));
    xhr.send(JSON.stringify({
        "type": "card",
        "number": document.getElementsByClassName("form-control")[0].value,
        "expiry_month" : document.getElementsByClassName("form-control")[1].value.substring(0,2),
        "expiry_year" : document.getElementsByClassName("form-control")[1].value.substring(5),
        "cvv" : document.getElementsByClassName("form-control")[2].value,
        "name" : document.getElementsByClassName("form-control")[3].value,
      }));
    xhr.onload = function() {
        var data = JSON.parse(this.responseText);
        if(data.status == "Authorized") {
            el.innerHTML = "Payment Complete!";
            var notify = new XMLHttpRequest();
            notify.open("POST", restUrl + "/notify_order", true);
            notify.setRequestHeader('Content-Type', 'application/json');
            notify.send(JSON.stringify({
                "messenger_id" : messenger_id,
                "blockName" : "Single Message Content",
                "messageContent" : "You successfully paid with the amount of\n" +
                                    total_price +
                                    " for items " +
                                    item_list.replace(/<br[/]>/g, "\n") +
                                    "\nThanks for your purchase!"
            }));
            ME.requestCloseBrowser(function() {
                console.log('Window will be closed');
            }, function(error) {
                console.log('There is an error');
                console.log(error);
            });
        } else {
            el.innerHTML = "Payment Error! Please try again";
            location.reload(); 
        }
    }*/
});

function expirationInputCheck() {
    if(document.getElementsByClassName("form-control")[1].value.length == 2 && !isNaN(document.getElementsByClassName("form-control")[1].value))
        document.getElementsByClassName("form-control")[1].value += "/"
    if(document.getElementsByClassName("form-control")[1].value == "/")
        document.getElementsByClassName("form-control")[1].value = ""      

}

function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;

    return true;
}    