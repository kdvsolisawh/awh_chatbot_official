//`1Diagchat
'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const requestPromise = require('request-promise');
const url = require('url');
const restService = express();
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const ngrok = require('ngrok');

const routerRegistration = require('./controller/registration');
const routerPersonal = require('./controller/personal');
const routerGeneral = require('./controller/general');
const routerHealthInfo = require('./controller/healthinfo');
const routerHouseholdInfo = require('./controller/householdinfo');
const routerSuggestion = require('./controller/suggestion');
const routerTester = require('./controller/tester');
const routerMaternalHealth = require('./controller/maternalhealth');
const routerChildHealth = require('./controller/childhealth');
const routerMentalHealth = require('./controller/mentalhealth');
const routerDisabilityInfo = require('./controller/disabilityinfo');
const routerValidator = require('./controller/validator');
const routerDispergo = require('./controller/dispergo');
const routerPayment = require('./controller/payment-pipeline');

var restUrl = "https://fbwebhook.alliedworld.healthcare";

routerDispergo.restUrl = restUrl;

restService.engine('html', require('ejs').renderFile); 

restService.use(bodyParser.urlencoded({
    extended: true
}));

restService.use(bodyParser.json());
/*****************************************************************************************/
restService.use(routerRegistration);
restService.use(routerPersonal);
restService.use(routerGeneral);
restService.use(routerHealthInfo);
restService.use(routerHouseholdInfo);
restService.use(routerSuggestion);
restService.use(routerTester);
restService.use(routerMaternalHealth);
restService.use(routerChildHealth);
restService.use(routerMentalHealth);
restService.use(routerDisabilityInfo);
restService.use(routerValidator);
restService.use(routerDispergo.subRouterDispergo);
restService.use(routerPayment);
/*****************************************************************************************/
/*(async function() {
    const url = await ngrok.connect(8000);
    console.log(url);
    restUrl = url;
    routerDispergo.restUrl = restUrl;
})();*/
restService.listen((process.env.PORT || 8000), () => console.log('Express server is listening on port 8000'));