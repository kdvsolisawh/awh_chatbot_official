const mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const couchbase = require('couchbase');
var dbUrl = "mongodb://localhost:27017/chatbotdb";

module.exports = {
    save_data: function(table, data) {
        console.log("save data");
        MongoClient.connect(dbUrl, { useNewUrlParser: true }, function(err, db) {
           if (err) throw err;
               var dbo = db.db("mydb");
               console.log(data);
               dbo.collection(table).insertOne(data, function(err, res) {
               if (err) throw err;
                  console.log("1 document inserted");
               db.close();
           });
      }); 
   },
   fetch_suggestions: function(callback, page) {
       MongoClient.connect(dbUrl,{ useNewUrlParser: true }, function(err, db) {
       if (err) throw err;
    	    var dbo = db.db("mydb");
	    dbo.collection("suggestions", function(err, collection){
		   collection.find().skip((page - 1)*10).limit(10).toArray(function(err, result) {
			if (err) throw err;
                        collection.find().count({ useNewUrlParser: true },function(err,count){
                	var pageTot = count *1.0/10;
                        return callback(result, pageTot);
	    	    });
		    db.close();
		});
        });
    }); 
   },
   fetch_user: function(callback, messenger_id, curis_id) {

	MongoClient.connect(dbUrl,{ useNewUrlParser: true }, function(err, db) {
  		if (err) throw err;
  		var dbo = db.db("mydb");
  		dbo.collection("dispergo_id").findOne({"messenger_id" : messenger_id, "curis_id" : curis_id}, function(err, result) {
    		    if (err) throw err;
    		    db.close();
                    if(result == null)
                       result = {};
    		    return callback(result);
  		});
	}); 
   },
   fetch_curis_id: function(callback, curis_id) {

	MongoClient.connect(dbUrl,{ useNewUrlParser: true }, function(err, db) {
  		if (err) throw err;
  		var dbo = db.db("mydb");
  		dbo.collection("registration").findOne({"curis_id" : curis_id}, function(err, result) {
    		    if (err) throw err;
    		    db.close();
                    if(result == null)
                       result = {};
    		    return callback(result);
  		});
	}); 
   },
   fetch_payment_id: function(callback, messenger_id, payment_id) {

	MongoClient.connect(dbUrl,{ useNewUrlParser: true }, function(err, db) {
  		if (err) throw err;
  		var dbo = db.db("mydb");
  		dbo.collection("payment_status").findOne({"messenger_id" : messenger_id, "payment_id" : payment_id}, function(err, result) {
    		    if (err) throw err;
    		    db.close();
                    if(result == null)
                       result = {};
    		    return callback(result);
  		});
	}); 
   },

	fetch_order_details: function(callback, type, order_id, currency) {
		var qty_list = "";
		var price_list = "";
		var total_price = 0.0;
		var item_list = "";

		var DB = JSON.parse(process.env.DB);
		var cluster = new couchbase.Cluster(DB.AWHDISPERGODB.COUCHBASE_N1QL_URI);
		cluster.authenticate("","");//DB.AWHDISPERGODB.COUCHBASE_N1QL_USERNAME, DB.AWHDISPERGODB.COUCHBASE_N1QL_PASSWORD);
		var bucket = cluster.openBucket(DB.AWHDISPERGODB.COUCHBASE_BUCKET);

		let statement = `SELECT awhdispergodb.*, meta().id FROM awhdispergodb WHERE type='` + type + `' and meta().id='` + order_id + `'`;
		let query = couchbase.N1qlQuery.fromString(statement);

		let promise = new Promise((resolve,reject) => {
			bucket.query(query, (error, response) => {
				if(error){
					console.log("bucket Error");
					reject(error)
				} 
				else {
					return resolve(response);
				}
			});
		}); 
		promise.then(function(result){
			var medCluster = new couchbase.Cluster(DB.AWHDISPERGODB.COUCHBASE_N1QL_URI);
			medCluster.authenticate("","");//DB.AWHDISPERGODB.COUCHBASE_N1QL_USERNAME, DB.AWHDISPERGODB.COUCHBASE_N1QL_PASSWORD);
			var medBucket = medCluster.openBucket(DB.AWHDISPERGODB.COUCHBASE_BUCKET);
			var medIDs = "";
			var patientName = result[0].patientName;
			var address = result[0].patientAddress;
			var elements = [];
			var medData = [];
			var otherDetails = {
				"ecr_link": "https://bit.ly/30UZrLg",
				"items": result[0].items,
				"patientAddress": address,
				"patientAge": result[0].patientAge,
				"patientGender": result[0].patientGender,
				"patientName": patientName,
				"physicianId": result[0].physicianId,
				"physicianLicenseNumber": result[0].physicianLicenseNumber,
				"physicianName": result[0].physicianName,
				"prescriptionNumber": result[0].prescriptionNumber,
				"residentId": result[0].residentId,
				"type": result[0].type
			}

			for(i in result[0].items) {
				medIDs += ("meta().id='" + result[0].items[i].medicationId + "'" + ((i + 1 < result[0].items.length)? " OR " : ""));
				qty_list += (result[0].items[i].quantity.toString() + ((i + 1 < result[0].items.length)? "<br/>" : ""));
			}

			let medStatement = `SELECT awhdispergodb.*, meta().id FROM awhdispergodb WHERE type='medication' AND (`+ medIDs + `)`;
			let query = couchbase.N1qlQuery.fromString(medStatement);

			let medPromise = new Promise((resolve,reject) => {
				medBucket.query(query, (error, response) => {
					if(error){
						console.log("medBucket Error");
						reject(error);
					} else {
						return resolve(response);
					}
				});
			});

			medPromise.then(function(result){
				for(i in result) {
					item_list += ("<a data-toggle=modal href=#myModal class=item-details data-id=" + i + ">" + result[i].name + "</a>" + ((i + 1 < result.length)? "<br/>" : ""));
					price_list += ((parseFloat(result[i].price)*parseFloat(qty_list.split("<br/>")[i])).toString() + ((i + 1 < result.length)? "<br/>" : ""));
					total_price += parseFloat(result[i].price)*parseFloat(qty_list.split("<br/>")[i]);
					elements.push({  
						"title": result[i].name,
						"subtitle": result[i].dosage,
						"quantity": qty_list.split("<br/>")[i],
						"price": ((parseFloat(result[i].price)*parseFloat(qty_list.split("<br/>")[i])).toString()),
						"currency":currency,
						"image_url":   "https://www.chemist-4-u.com/media/catalog/product/cache/1/image/450x/9df78eab33525d08d6e5fb8d27136e95/p/a/paracetamol_tablets_500mg/Paracetamol_Tablets_500mg_-_32_Tablets_(Brand_May_Vary)_31.jpg"
						});
					medData.push({
						"dosage": result[i].dosage,
						"form": result[i].form,
						"ingredients": result[i].ingredients,
						"name": result[i].name,
						"packageSize": result[i].packageSize,
						"price": result[i].price
						});
					}
					return callback(patientName, item_list, qty_list, price_list, address, JSON.stringify(elements), total_price.toFixed(2).toString(), medData, otherDetails);
			}).catch(function(err){
				console.log("medPromise Error");
				return callback();
			});
		}).catch(function(err){
			console.log("promise Error");
			return callback();
		});
	}
}