const express = require('express');
const routerValidator = require('express').Router();

var errorInputMessage = {
                          "Eng" : ["What you've entered does not look like a phone number. Please try again.",
                                   "What you've entered does not look like an email. Please try again.",
				   "What you've entered does not look like a number. Please try again."],
                          "Hil" : ["Daw indi cellphone number ang imo gin-enter. Palihog check kag enter liwat.",
                                   "Daw indi email address ang imo gin-enter. Palihog check kag enter liwat.",
				   "Daw indi tama nga number ang imo gin-enter. Palihog check kag enter liwat."]
}

routerValidator.post('/validator', function(req, res) {
    console.log("routerValidator post");
    console.log(req.body);
    var lang = req.body.lang.substring(0,3);
    var messageType = parseInt(String(req.body.messagetype));
    var attribute = req.body.attribute;
    var answer = req.body[attribute];
    var block = req.body.block;
    var jsonResponse = {};
    if(messageType == 0) {
         if(isNaN(answer) || answer.length < 7) {
             jsonResponse = {
 	          "messages": [
                      {"text": errorInputMessage[lang][messageType]}
                  ],
                  "redirect_to_blocks": [lang + block]
             }
         }
    }
    if(messageType == 1) {
         if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(answer))) {
             jsonResponse = {
 	          "messages": [
                      {"text": errorInputMessage[lang][messageType]}
                  ],
                  "redirect_to_blocks": [lang + block]
             }
         }
         
    }
    if(messageType == 2) {
         if(isNaN(answer)) {
             jsonResponse = {
 	          "messages": [
                      {"text": errorInputMessage[lang][messageType]}
                  ],
                  "redirect_to_blocks": [lang + block]
             }
         }
         
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

module.exports = routerValidator ;