const express = require('express');
const routerTester = require('express').Router();
const model = require("../model/datahandler");
const url = require('url');
const requestPromise = require('request-promise');
const chatfuelBroadcast = require('chatfuel-broadcast').default;
const mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const dbUrl = "mongodb://localhost:27017/chatbotdb";

routerTester.post('/get_info', function(req, res) {
    console.log("routerTester post");
    console.log(req.body);
   
    var jsonResponse = {};
    MongoClient.connect(dbUrl, { useNewUrlParser: true }, function(err, db) {
  	if (err) throw err;
  	var dbo = db.db("mydb");
 	dbo.collection(req.body.infotype).findOne(req.body.query, function(err, result) {
    		if (err) throw err;
                jsonResponse = result;
    		console.log(jsonResponse);
    		res.send(jsonResponse);
    		db.close();
  		});
	}); 
});

routerTester.post('/clear_all', function(req, res) {
    console.log("routerTester post");
    console.log(req.body);
	MongoClient.connect(dbUrl, { useNewUrlParser: true }, function(err, client) { 
        	if (err) throw err; 
        	console.log("Connected to Database!");
        	console.log("db object points to the database : "+ client.db.databaseName); 
        	client.db("mydb").dropDatabase(function(err, result){ 
        		console.log("Error : "+err); 
        		if (err) throw err; 
        		res.send({"status":"Operation Success ? "+result}); 
        		client.close(); 
    		});
	});
});


routerTester.post('/object_test', function(req, res) {
    console.log("routerTester post");
	console.log(req.body);
	object = JSON.parse(req.body.object)
	res.send({
		"messages": [
		  {"text": object.obj}
		]
	   });
});

module.exports = routerTester;