const express = require('express');
const routerMentalHealth = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerMentalHealth.post('/mental_health', function(req, res) {
    console.log("routerMentalHealth post");
    var lang = req.body.lang.substring(0,3);
    var child_med = req.body.child_med;
    var child_illness = req.body.child_illness;
    var baby_med_proc = req.body.baby_med_proc;
    var child_block = req.body.child_block;
    var jsonResponse = {};
    
    if(typeof self_mental_illness !== "undefined") {
        if(self_mental_illness.split(",").some(isNaN) &&
           self_mental_illness.toLowerCase() != "none" &&
           self_mental_illness.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Mental Health 2"]
			};
        } else {
            jsonResponse = {};
        }
    }
    
    if(typeof others_mental_illness !== "undefined") {
        if(others_mental_illness.split(",").some(isNaN) &&
           others_mental_illness.toLowerCase() != "none" &&
           others_mental_illness.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Mental Health 3"]
			};
        } else {
            jsonResponse = {};
        }
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerMentalHealth.post('/save_mental_health', function(req, res) {
    console.log("routerMentalHealth post");   
    var jsonResponse = {};

    model.save_data("mental_health", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your mental health info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerMentalHealth ;